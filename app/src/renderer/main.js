import Vue from 'vue'
import Electron from 'vue-electron'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import Resource from 'vue-resource'
import Router from 'vue-router'
import App from './App'
import routes from './routes'

Vue.use(Electron)
Vue.use(Resource)
Vue.use(Router)
Vue.use(ElementUI)
Vue.config.debug = true

Vue.prototype.getApiServer = function (option) {
  if (option.debug) {
    return 'http://localhost:14492/api/'
  }
  return 'http://localhost:8001/api/'
}

let setCookie = (cname, value, expiredays) => {
  var exdate = new Date()
  exdate.setDate(exdate.getDate() + expiredays)
  document.cookie = cname + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString())
}

let getCookie = (cname) => {
  const cookie = document.cookie
  if (cookie.length > 0) {
    let hasCookie = cookie.includes(cname + '=')
    if (hasCookie) {
      let cstart = cname.length + 1
      let cend = cookie.indexOf(';', cstart)
      if (cend === -1) cend = cookie.length
      return unescape(cookie.substring(cstart, cend))
    }
  }
  return ''
}

Vue.prototype.setCookie = setCookie
Vue.prototype.getCookie = getCookie

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  ...App
}).$mount('#app')
