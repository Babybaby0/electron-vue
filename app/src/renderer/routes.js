import store from './vuex/store'

export default [
  {
    path: '/login',
    name: 'login-page',
    component: require('components/LoginView'),
    beforeEnter: (to, from, next) => {
      let loginState = store.getters.loginState
      console.log(loginState)
      if (loginState === 1) {
        // 登录状态已经为1的时候从哪来回哪去
        next('/#/main')
      } else {
        next()
      }
    }
  },
  {
    path: '/signup',
    name: 'signup-page',
    component: require('components/SignupView'),
    beforeEnter: (to, from, next) => {
      let loginState = store.getters.loginState
      if (loginState === 1) {
        // 登录状态已经为1的时候从哪来回哪去
        next('/#/mian')
      } else {
        next()
      }
    }
  },
  {
    path: '/main',
    name: 'main-page',
    component: require('components/MainView'),
    // 路由钩子判断是否登录
    beforeEnter: (to, from, next) => {
      // let loginState = store.getters.loginState
      let loginUser = store.getters.loginUser
      console.log(loginUser)
      /* if (loginState === 0) {
        // 未登录进入主页返回登录页面
        next('/#/login')
      } else if (loginState === 1) {
        next()
      } */
      next()
    }
  },
  {
    path: '*',
    redirect: '/main'
  }
]
