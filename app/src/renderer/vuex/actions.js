import * as types from './mutation-types'

export const decrementMain = ({ commit }) => {
  commit(types.DECREMENT_MAIN_COUNTER)
}

export const incrementMain = ({ commit }) => {
  commit(types.INCREMENT_MAIN_COUNTER)
}

export const userLogin = ({ commit }, loginUser) => {
  commit(types.USER_LOGIN, loginUser)
}

export const userLogout = ({ commit }) => {
  commit(types.USER_LOGOUT)
}
