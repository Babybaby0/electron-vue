import * as types from '../mutation-types'

const state = {
  loginState: 0,
  loginUser: null
}

const mutations = {
  [types.USER_LOGIN] (state, user) {
    state.loginState = 1
    state.loginUser = user
  },
  [types.USER_LOGOUT] (state) {
    state.loginState = 0
    state.loginUser = null
  }
}

export default {
  state,
  mutations
}
