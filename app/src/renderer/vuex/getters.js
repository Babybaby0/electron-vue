export const mainCounter = state => state.counters.main

export const loginState = state => state.login.loginState

export const loginUser = state => state.login.loginUser
